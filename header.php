<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" >
        <meta name="viewport" content="width=device-width">

		<?php wp_head(); ?>

	</head>

    <body <?php body_class(); ?>>
		<div id="wrapper" class="wrapper">

			<header>
				<div class="logo-wrapper row column-12">
					<?php nuts_logo(); ?>
				</div>
				<div class="row column-12">
				    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'primary',
                        'menu_id' => 'primary_menu',
                        'container' => 'nav',
                        'items_wrap' => '<ul id="%1$s" class="%2$s desktop-only">%3$s</ul>',
                    ) );
                    nuts_hamburger_icon( 'primary_menu', 'mobile-only' );
                    ?>
				</div>
			</header>
