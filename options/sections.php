<?php


// Set up the sections in Theme Options

$section = array (
	"name"			=> 'header',
	"title"			=> __( 'Header', 'nuts-starter' ),
	"description"	=> __( 'Options for the header, logo, etc.', 'nuts-starter' ),
	"tab"			=> __( 'Header', 'nuts-starter' )
);
nuts_register_section ( $section );


$section = array (
	"name"			=> 'colors',
	"title"			=> __( 'Colors', 'nuts-starter' ),
	"description"	=> __( 'Set up the theme colors here', 'nuts-starter' ),
	"tab"			=> __( 'Colors', 'nuts-starter' )
);
nuts_register_section ( $section );


$section = array (
	"name"			=> 'other',
	"title"			=> __( 'Other options', 'nuts-starter' ),
	"description"	=> __( 'Some miscellaneous options', 'nuts-starter' ),
	"tab"			=> __( 'Other', 'nuts-starter' )
);
nuts_register_section ( $section );


// And some custom field sections for posts

$section = array (
	"name"			=> 'post::post_options',
	"title"			=> __( 'Post Options', 'nuts-starter' ),
	"description"	=> __( 'This is an example metabox section', 'nuts-starter' ),
);
nuts_register_section ( $section );
