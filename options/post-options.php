<?php

// Text field
$nuts_option_array = array (
        "name"			=> 'source',
        "title"			=> __( 'Source link', 'nuts-starter' ),
        "description"	=> __( 'Source URL of the original article.', 'nuts-starter' ),
        "section"		=> 'post::post_options',
        "type"			=> 'text',
        "size"			=> '',
        "placeholder"   => __( 'Enter a URL here', 'nuts-starter' ),
);
nuts_register_option ( $nuts_option_array );
