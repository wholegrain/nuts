<?php

// The file must have the type-[data-type].php filename format



// The function must have the nuts_type_[data-type]_field name format
function nuts_type_number_field ( $name, $value ) {

	global $nuts_options_array;

	if ( isset( $nuts_options_array[$name]["step"] ) && $nuts_options_array[$name]["step"] != NULL ) $step = $nuts_options_array[$name]["step"];

	if ( isset( $nuts_options_array[$name]["min"] ) && $nuts_options_array[$name]["min"] != NULL ) $min = $nuts_options_array[$name]["min"];

	if ( isset( $nuts_options_array[$name]["max"] ) && $nuts_options_array[$name]["max"] != NULL ) $max = $nuts_options_array[$name]["max"];

	if ( isset( $nuts_options_array[$name]["prefix"] ) ) $prefix = $nuts_options_array[$name]["prefix"];
		else $prefix = '';
	if ( isset( $nuts_options_array[$name]["suffix"] ) ) $suffix = $nuts_options_array[$name]["suffix"];
		else $suffix = '';

	if ( isset( $nuts_options_array[$name]["placeholder"] ) ) $placeholder = $nuts_options_array[$name]["placeholder"];
		else $placeholder = '';

	if ( $value == "" ) $value = $nuts_options_array[$name]["default"];

	echo '<div class="number">
			' . $prefix . '<input type="number" name="' . nuts_form_ref( $name ) . '" id="' . $name . '" value="' . esc_attr( $value ) . '"';

	if ( isset( $min ) && $min != NULL ) echo ' min="' . $min . '"';
	if ( isset( $max ) && $max != NULL ) echo ' max="' . $max . '"';
	if ( isset( $step ) && $step != NULL ) echo ' step="' . $step . '"';
	if ( isset( $placeholder ) && $placeholder != '' ) echo ' placeholder="' . $placeholder . '"';

	echo ' />' . $suffix . '
		</div>';

}



// Returns the number value. Adds the prefix before and the suffix after the number. If you add the 2nd parameter true, you'll get the pure number, without prefix and suffix
function nuts_get_number ( $name, $pure = false ) {

	global $nuts_options_array;

	if ( isset( $nuts_options_array[$name]["prefix"] ) ) $prefix = $nuts_options_array[$name]["prefix"];
		else $prefix = '';
	if ( isset( $nuts_options_array[$name]["suffix"] ) ) $suffix = $nuts_options_array[$name]["suffix"];
		else $suffix = '';

	if ( !intval( nuts_get_option( $name ) ) ) {
		return 0;
	}

	if ( $pure == false ) {
		return $prefix . nuts_get_option( $name ) . $suffix;
	}
	else {
		nuts_get_option ( $name );
	}

}




// Prints the number value
function nuts_number ( $name ) {

	echo esc_html( nuts_get_number ( $name ) );

}
