<?php get_header(); ?>

<div id="contentWrapper" class="row">
    <div id="content" class="blog content column-8">

		<?php get_template_part ( 'loop', 'blog' ); ?>

		<aside class="pagination">
			<?php next_posts_link( nuts_get_value ( 'olderposts' ) ); ?>
			<?php previous_posts_link( nuts_get_value ( 'newerposts' ) ); ?>
		</aside>

    </div><!-- content -->

    <?php get_sidebar(); ?>

</div><!-- contentWrapper -->

<?php get_footer();
